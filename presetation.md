---
title: "Why Rust Is Worth It: Fearless Concurrency No Matter The Paradigm"
date: 2021-12-09
author: Michael Edlinger
extensions:
  - terminal
styles:
  style: monokai
  table:
    column_spacing: 5
  margin:
    top: 2
    bottom: 0
  padding:
    top: 2
    bottom: 2
---

# Who am I?

Michael Edlinger aka @mkeedlinger

Tech enthusiast and software engineer.

---

Worth it??

Are you implying Rust is not the best just all the time??

**Yep. Rust may not always be the best choice.**

Correctness and performance aren't always paramount, but when they are, Rust is killer.

---

# What we'll be doing today

## Looking at a few ways to solve a problem

- 3N+1
- aka the Collatz Conjecture
- Veritasium's awesome video: https://youtu.be/094y1Z2wpJg

The problem, as simply as possible:

```python
# For any positive int n
# this algorithm ⇓

while n != 1:
  if n % 2 == 0:
    n = n / 2
  else:
    n = (3 * n) + 1

# will reach 1
```

## Explore different ways to verify the conjecture for some ints

- One Thread
- Multiple Threads
- Multiple Computers, aka a network service (#thecloud, Collatz as a Service™)
  - Async Rust!

What performance characteristics does each have?

---

# Single Thread

## In JS

```file
path: ./code/single_thread/single.js
lang: js
```

```
hyperfine 'node ./single.js'
```

```terminal10
fish -C "cd code/single_thread"
```

## In Rust

```file
path: ./code/single_thread/src/main.rs
lang: rust
transform: awk '/\/\/ 1-/{f=1;next} /\/\/ 1/{f=0} f'
```

```
cargo build --release
hyperfine ./target/release/single_thread
```

```terminal10
fish -C "cd code/single_thread"
```

...

...

... that's fast. Maybe too fast.

Did the loop get optimized away?

TL;DR: Nope. The assembly (as shown on https://gcc.godbolt.org/, an awesome tool) keeps the loop.

I'm not sure how it's that fast, it actually hurts my point which was:

---

# Garbage Collected languages are Often Good Enough™

Debian's Computer Language Benchmarks Game (https://benchmarksgame-team.pages.debian.net/benchmarksgame/index.html) has some stats:

| Benchmark          | Rust  | Go     | Java  | JavaScript |
| ------------------ | ----- | ------ | ----- | ---------- |
| pidigits           | 0.71s | 1.00s  | 0.93s | 1.31s      |
| regex-redux        | 0.77s | 3.85s  | 5.31s | 4.82s      |
| mandelbrot         | 0.93s | 3.73s  | 4.12s | 4.04s      |
| binary-trees       | 1.09s | 12.23s | 2.48s | 7.20s      |
| reverse-complement | 0.45s | 1.35s  | 1.53s | 2.21s      |
| ...                |       |        |       |            |

Compared to my Collatz Conjecture test, I think these are more representative of how well some GC langs can perform.

They're not too far off.

**So where does Rust start to kick butt?**

Fearless Concurrency!

---

# Concurrency and Parallelism can be hard

Let's try to test the Collatz Conjecture over many threads.

Trying to get any form of parallelism in JS is a pain / not feasible / not worth it (just ask @janka102, who attempted to write code for this problem in multiprocessed node), so we're switching to Go.

## Go

```file
path: ./code/multi_naive/multi.go
lang: go
transform: bash -c "awk '/\/\/ 1-/{f=1;next} /\/\/ 1/{f=0} f' | sed 's+\t+    +g'"
```

- Is this code correct?
  - Will it compile?
- What will the output be?
  - Is it deterministic?

```
go build multi.go
go run multi.go
```

```terminal15
fish -C "cd code/multi_naive"
```

## Rust

We're going to try the same thing.

```file
path: ./code/multi_naive/src/main.rs
lang: rust
transform: awk '/\/\/ 1-/{f=1;next} /\/\/ 1/{f=0} f'
```

Same issue?

(`cargo build` in separate terminal)

---

# How does Rust save us? Ownership (1/3)

Ownership has three rules (copied from The Rust Book):

- Each value in Rust has a variable that’s called its owner.
- There can only be one owner at a time.
- When the owner goes out of scope, the value will be dropped.

Also rules for references: **one** mutable reference XOR any number of read-only references.

```rust
let mut owner: Vec<usize> = Vec::new();

let mut_ref = &mut owner; // fine

let ro_ref = &owner; // ❌
```

This helps answer:

- Who can mutate?
- What is responsible for cleaning up? When?

---

# How does Rust save us? Traits (2/3)

## What are traits?

Traits are not too unlike interfaces.

There are special traits called Marker Traits.

- Managed by the compiler (ie, you don't implement them)
- Represent properties of some types

## Sync & Send

Both are Marker traits.

### Send

Answers: Can this type be sent across threads?

- Example: Arc.
- Counter example: Rc.
  - Why? Doesn't atomically synchronize ref counts

### Sync

"Types for which it is safe to share references between threads." (STD docs)

Answers: Can this type manage mutations from multiple threads?

- Example: Mutex.
- Counter example: Cell.
  - Why? Provides interior mutability w/o synchronization

---

# How does Rust save us? RAII (3/3)

RAII == Resource Acquisition Is Initialization

## Mutex in Go

```file
path: ./code/multi_threaded/multi.go
lang: go
transform: bash -c "awk '/\/\/ 2-/{f=1;next} /\/\/ 2/{f=0} f' | sed 's+\t+    +g'"
```

What's wrong? You can use `next` without properly "initializing" (read: setting things up).

This code compiles just as well when forgetting the Mutex!

## Mutex in Rust

```file
path: ./code/multi_threaded/src/main.rs
lang: rust
transform: awk '/\/\/ 2-/{f=1;next} /\/\/ 2/{f=0} f'
```

```file
path: ./code/multi_threaded/src/main.rs
lang: rust
transform: awk '/\/\/ 3-/{f=1;next} /\/\/ 3/{f=0} f'
```

- The `u64` can only be mutated through the MutexGuard.
- The MutexGuard can only by **acquired** by `.lock`ing, ensuring things have been properly **initialized**.

Not impossible to misuse. Deadlock still possible, but memory corruption is not.

---

# Correct Implementations

Now we can see how these things come together!

## Go

```file
path: ./code/multi_threaded/multi.go
lang: go
transform: bash -c "awk '/\/\/ 1-/{f=1;next} /\/\/ 1/{f=0} f' | sed 's+\t+    +g'"
```

```
go build multi.go
hyperfine ./multi
```

```terminal10
fish -C "cd code/multi_threaded"
```

## Rust

```file
path: ./code/multi_threaded/src/main.rs
lang: rust
transform: awk '/\/\/ 1-/{f=1;next} /\/\/ 1/{f=0} f'
```

```
cargo build --release
hyperfine ./target/release/multi_threaded
```

```terminal10
fish -C "cd code/multi_threaded"
```

No GC is fast!

## Small Aside

The Rust allows for confident use of third party libraries like `rayon`, which could make our code as simple as:

```file
path: ./code/multi_threaded/src/rayon.rs
lang: rust
transform: awk '/\/\/ 1-/{f=1;next} /\/\/ 1/{f=0} f'
```

---

# "Ok but I just care about the #cloud"

Fair. A lot of code is about networked services.

## Also..

"I keep hearing a lot about async Rust."

\- You, probably

## Yay, async

But, why?

Networked services need to process many user requests concurrently. One way to do that is with threads, but that is usually inefficient.

- Threads can be expensive to create
  - OS kernel must manage them
  - Each one needs its own stack space
- Each thread can effectively only handle one request at a time, since IO is blocking

Async code can alleviate these issues.

- Runtime manages "tasks" (called `futures` in Rust)
- Runtime can do other tasks while a task waits for IO

## An Example (shamelessly taken from the Rust Async Book)

```rust
fn get_two_sites() {
    // Spawn two threads to do work.
    let thread_one = thread::spawn(|| download("https://www.foo.com"));
    let thread_two = thread::spawn(|| download("https://www.bar.com"));

    // Wait for both threads to complete.
    thread_one.join().expect("thread one panicked");
    thread_two.join().expect("thread two panicked");
}
```

vs

```rust
async fn get_two_sites_async() {
    // Create two different "futures" which, when run to completion,
    // will asynchronously download the webpages.
    let future_one = download_async("https://www.foo.com");
    let future_two = download_async("https://www.bar.com");

    // Run both futures to completion at the same time.
    join!(future_one, future_two);
}
```

## Is it really much faster?

Yes. So so much faster.

I made a little test where I spawn 10,000 threads, and `.await` 10,000 futures (each doing nothing).

Threads took 357ms.

Async took 20µs (microseconds).

---

**Disclaimer: I am no Async Rust expert, now we're in the weeds**

Also:

> In short, async Rust is more difficult to use and can result in a higher maintenance burden than synchronous Rust, but gives you best-in-class performance in return.

\- The Rust Async Book

---

# Rust Async Speed Course

- Futures
  - Anything that implements the `Future` trait
  - Unit of work
  - Not too unlike JS Promises
  - Part of the Standard Library
- Executors / Runtime
  - Poll futures, progressing them
  - **Not in stdlib, no blessed implementation**
  - Tokio is currently the most popular
- `async` / `.await`
  - Desugar down to a state machine of futures that the executor can use

---

# Let's build a Collatz Service!

First we have some dependencies to get:

`Cargo.toml`

```file
path: ./code/async_collatz/Cargo.toml
lang: toml
transform: awk '/# 1-/{f=1;next} /# 1/{f=0} f'
```

--

What should our service look like? Make a gRPC service definition!

`proto/service.proto`

```file
path: ./code/async_collatz/proto/service.proto
lang: proto
```

--

Cargo can run build steps for us via the `build.rs` file:

```file
path: ./code/async_collatz/build.rs
lang: rust
```

--

This generates a Rust module we can use

`main.rs`

```file
path: ./code/async_collatz/src/main.rs
lang: rust
transform: awk '/\/\/ use-gen-/{f=1;next} /\/\/ use-gen/{f=0} f'
```

```file
path: ./code/async_collatz/src/main.rs
lang: rust
transform: awk '/\/\/ gen-mod-/{f=1;next} /\/\/ gen-mod/{f=0} f'
```

---

# Collatz Service (part 2)

All that just so we can do this:

```file
path: ./code/async_collatz/src/main.rs
lang: rust
transform: awk '/\/\/ compute-fn-/{f=1;next} /\/\/ compute-fn/{f=0} f'
```

(Ok, I lied, there's a tad more boilerplate there)

---

# Collatz Service (in action!)

(run in another terminal)

---

# Thanks for listening!

The materials for this presentation can be found here:
https://gitlab.com/mkeedlinger/rust-concurrency-presentation/

## Some Other (better) Resources

**The Rust Book**: https://doc.rust-lang.org/stable/book/

Rust Async Book: https://rust-lang.github.io/async-book/

Jon Gjengset's "Crust of Rust" Videos: https://www.youtube.com/watch?v=rAl-9HwD858&list=PLqbS7AVVErFiWDOAVrPt7aYmnuuOLYvOa

Jon Gjengset's book "Rust for Rustaceans: https://nostarch.com/rust-rustaceans

Rust Standard Library Docs: https://doc.rust-lang.org/std/

Rustlings?: https://github.com/rust-lang/rustlings/
