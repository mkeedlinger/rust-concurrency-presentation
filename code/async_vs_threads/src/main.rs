use std::thread::spawn;
use std::time::Instant;

const NUM: usize = 10_000;

fn main() {
    let start = Instant::now();

    for _i in 0..NUM {
        spawn(|| {}).join().unwrap();
    }

    println!("Threads took {:?}", start.elapsed());

    tokio::runtime::Builder::new_current_thread()
        .enable_all()
        .build()
        .unwrap()
        .block_on(async {
            let start = Instant::now();
            for _i in 0..NUM {
                do_nothing().await
            }
            println!("Async took {:?}", start.elapsed());
        })
}

async fn do_nothing() {}
