fn main() {
    // 1-
    for mut n in 1..=1_000_000u64 {
        while n != 1 {
            if n % 2 == 0 {
                n /= 2;
            } else {
                n = 3 * n + 1;
            }
        }
    }
    // 1
}
