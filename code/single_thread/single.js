for (let i = 1; i <= 1_000_000; i++) {
  let n = i;
  while (n !== 1) {
    if (n % 2 === 0) {
      n /= 2;
    } else {
      n = 3 * n + 1;
    }
  }
}
