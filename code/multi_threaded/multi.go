package main

import (
	"runtime"
	"sync"
)

// 1-
const UP_TO uint64 = 1_000_000

func main() {
	var next uint64 = 1
	var lock sync.Mutex
	var wg sync.WaitGroup

	for i := 0; i < runtime.NumCPU(); i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()

			for {
				// 2-
				lock.Lock()
				num := next
				next++
				lock.Unlock()
				// 2

				if num > UP_TO {
					break
				}
				collatz(num)
			}
		}()
	}

	wg.Wait()
}

// 1

func collatz(num uint64) {
	for {
		if num == 1 {
			break
		}
		if num%2 == 0 {
			num = num / 2
		} else {
			num = 3*num + 1
		}
	}
}
