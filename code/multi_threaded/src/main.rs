use num_cpus;
use std::sync::{Arc, Mutex};
use std::thread::spawn;

mod rayon;

// 1-
const UP_TO: u64 = 1_000_000;

fn main() {
    // 2-
    let next: Arc<Mutex<u64>> = Arc::new(Mutex::new(1));
    // 2
    let cpus = num_cpus::get();
    let mut threads = Vec::with_capacity(cpus);

    for _core in 0..cpus {
        threads.push(spawn({
            let next = Arc::clone(&next);
            move || loop {
                let num;
                {
                    // 3-
                    let mut next_guard = next.lock().unwrap();
                    num = *next_guard;
                    *next_guard += 1;
                    // 3
                }

                if num > UP_TO {
                    break;
                }
                collatz(num);
            }
        }));
    }

    for thread in threads.drain(..) {
        thread.join().unwrap();
    }
}
// 1

pub fn collatz(mut num: u64) {
    loop {
        if num == 1 {
            break;
        }
        if num % 2 == 0 {
            num = num / 2;
        } else {
            num = 3 * num + 1;
        }
    }
}
