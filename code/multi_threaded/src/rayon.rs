use super::{collatz, UP_TO};
use rayon::prelude::*;

#[allow(unused)]
fn with_rayon() {
    // 1-
    (1..=UP_TO).into_par_iter().for_each(|num| {
        collatz(num);
    });
    // 1
}

#[test]
fn test_with_rayon() {
    with_rayon();
}
