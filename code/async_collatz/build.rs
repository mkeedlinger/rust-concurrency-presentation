fn main() -> Result<(), Box<dyn std::error::Error>> {
    // Compiles our service definition to Rust code
    tonic_build::compile_protos("proto/service.proto")?;
    Ok(())
}
