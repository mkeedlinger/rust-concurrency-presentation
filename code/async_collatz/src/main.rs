// use-gen-
use collatz::collatz_server::{Collatz, CollatzServer};
use collatz::{Range, Reply};
// use-gen
use rayon::iter::{IntoParallelIterator, ParallelIterator};
use std::collections::HashMap;
use std::error::Error;
use std::time::Instant;
use tonic::{transport::Server, Request, Response, Status};

// gen-mod-
pub mod collatz {
    tonic::include_proto!("collatz");
}
// gen-mod

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let addr = "[::1]:50051".parse().unwrap();
    let collatz_service = CollatzService::default();

    println!("CollatzService listening on {}", addr);

    Server::builder()
        .add_service(CollatzServer::new(collatz_service))
        .serve(addr)
        .await?;

    Ok(())
}

#[tonic::async_trait]
impl Collatz for CollatzService {
    // compute-fn-
    async fn compute(&self, request: Request<Range>) -> Result<Response<Reply>, Status> {
        let start = Instant::now();

        let range = request.into_inner();

        let calculate = tokio::task::spawn_blocking(move || {
            (range.start..=range.end)
                .into_par_iter()
                .for_each(|mut num| {
                    while num != 1 {
                        if num % 2 == 0 {
                            num /= 2;
                        } else {
                            num = 3 * num + 1;
                        }
                    }
                });
        });

        let message = get_cat_fact();

        let (message, _) = tokio::join!(message, calculate);

        match message {
            Ok(message) => {
                let reply = Reply {
                    millis: start.elapsed().as_millis() as u64,
                    message,
                };
                Ok(Response::new(reply))
            }
            Err(_e) => Err(Status::unknown("Failed")),
        }
    }
    // compute-fn
}

#[derive(Default)]
struct CollatzService {}

async fn get_cat_fact() -> Result<String, Box<dyn Error + Send + Sync>> {
    let mut res = reqwest::get("https://meowfacts.herokuapp.com/")
        .await?
        .json::<HashMap<String, Vec<String>>>()
        .await?;

    let res = res
        .remove("data")
        .unwrap_or_default()
        .pop()
        .unwrap_or_default();

    Ok(res)
}
