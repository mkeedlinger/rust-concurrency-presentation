package main

import (
	"runtime"
	"sync"
)

// 1-
const UP_TO uint64 = 3

func main() {
	var next uint64 = 1
	var wg sync.WaitGroup

	for i := 0; i < runtime.NumCPU(); i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()

			for {
				num := next

				if num > UP_TO {
					break
				}
				println(num)
				collatz(num)

				next++
			}
		}()
	}

	wg.Wait()
}

// 1

func collatz(num uint64) {
	println(num)
	for {
		if num == 1 {
			break
		}
		if num%2 == 0 {
			num = num / 2
		} else {
			num = 3*num + 1
		}
	}
}
