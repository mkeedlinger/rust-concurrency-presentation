use num_cpus;
use std::thread::spawn;

// 1-
const UP_TO: u64 = 1_000_000;

fn main() {
    let cpus = num_cpus::get();

    let mut next: u64 = 1;
    let mut threads = Vec::with_capacity(cpus);

    for _core in 0..cpus {
        threads.push(spawn(|| loop {
            let num = next;

            if num > UP_TO {
                break;
            }
            println!("{}", num);
            collatz(num);

            next += 1;
        }));
    }

    for thread in threads.drain(..) {
        thread.join().unwrap();
    }
}
// 1

fn collatz(mut num: u64) {
    loop {
        if num == 1 {
            break;
        }
        if num % 2 == 0 {
            num = num / 2;
        } else {
            num = 3 * num + 1;
        }
    }
}
